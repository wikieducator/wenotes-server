/*
 * a simple web and Faye server for WEnotes
 *
 * Copyright 2016 Open Education Resource Foundation
 * @license MIT
 */
/* jshint node:true, esversion:6 */
"use strict";

var util = require('util'),
    fs = require('fs'),
    http = require('http'),
    https = require('https'),
    Log = require('log'),
    nodeStatic = require ('node-static'),
    url = require('url'),
    faye = require('faye'),
    cradle = require('cradle'),
    options = require('../options.json');

var log = new Log(options.log.server.level,
              fs.createWriteStream(options.log.server.file, {
                flags: 'a'}));

// split http://user:pass@host:port into a map of its parts
function getConnectionMap(cs) {
  var r = {},
      parts = cs.split('@');
  if (parts.length === 2) {
    let userpass = parts[0].replace(/https?:\/\//, '').split(':');
    if (userpass.length === 2) {
      r.user = userpass[0];
      r.pass = userpass[1];
    }
    parts = parts[1].split(':');
    if (parts.length === 2) {
      r.host = parts[0];
      r.port = parts[1];
    }
  }
  return r;
}

var cdb = getConnectionMap(options.url),
    couch = new(cradle.Connection)(cdb.host, cdb.port, {
      auth: {username: cdb.user, password: cdb.pass}}),
    db = couch.database(options.db);

process.addListener('uncaughtException', (err, stack) => {
  log.alert('Exception: ' + err);
  log.alert(err.stack);
});

var ServerAuth = {
  incoming: function(message, callback) {
    if (/^\/meta\//.test(message.channel)) {
      if ((message.channel === '/meta/subscribe') || (message.channel === '/meta/unsubscribe')) {
        // log channel subscriptions and unsubscriptions
        log.info(message.channel, message);
      }
      return callback(message);
    }

    // limit publishing to people that know APP_PASSWORD
    var password = message.ext && message.ext.password;
    if (password !== options.fayepass) {
      log.warning('bad password, message discarded', message);
      message.error = faye.Error.extMismatch();
      return;
    }
    if (password) {
      // don't pass the password along
      delete message.ext.password;
    }
    callback(message);
  }
};

var ClientAuth = {
  outgoing: function(message, callback) {
    message.ext = message.ext || {};
    message.ext.password = options.fayepass;
    callback(message);
  }
};


function createBayeuxServer(mount) {
  var bayeux = new faye.NodeAdapter({
    mount: mount || '/faye',
    timeout: 45
  });

  return bayeux;
}

// FIXME: remove RocketChatisms like bold/italic markers?
function cleanText(t) {
  // remove :xxx: style emoji
  return t.replace(/:[_a-z]+:/g, '');
}

/* incoming webhook integration */
function saveNote(b) {
  var datetime = new Date(b.timestamp),
      note = {
        created_at: datetime.toString(),
        from_user: b.user_name,
        from_user_name: b.user_name,
        profile_image_url: `https://chat.oeru.org/avatar/${encodeURIComponent(b.user_name)}.jpg`,
        profile_url: `https://chat.oeru.org/direct/${encodeURIComponent(b.user_name)}`,
        text: cleanText(b.text),
        we_source: 'chat',
        url: `${options.RocketChat.baseURL}channel/${encodeURIComponent(b.channel_name)}`,
        we_tags: [ options.RocketChat.tag ],
        we_timestamp: b.timestamp
      };

  log.debug('=============incoming webhook================');
  log.debug(b);
  db.save(note);
}

function httpHandler(request, response) {
  log.debug(`handle ${request.method} request ${request.url}`);
  var file = new nodeStatic.Server('./public', {
    cache: false
  });
  var body = [];

  request.on('error', (e) => {
    log.critical('HTTP request error', e);
  }).on('data', (d) => {
    body.push(d);
  }).on('end', () => {
    var couchURL, jsonp, jsonString,
      location = url.parse(request.url, true),
      params = (location.query || request.headers);

    if (body.length) {
      body = Buffer.concat(body).toString();
      log.debug('BODY:', body);
    }

    // simple port 80 proxy to CouchDB
    if (location.pathname.indexOf('/DB/') === 0 && request.method === 'GET') {
      jsonp = params && params.callback;
      couchURL = options.url + request.url.slice(3);
      log.debug('***GET DB ' + location.pathname, couchURL);

      http.get(couchURL, (res) => {
        var data = '';
        response.writeHead(res.statusCode, res.headers);
        res.setEncoding('utf8');
        res.on('data', (d) => {
          data += d;
        });
        res.on('end', () => {
          response.write(data);
          response.end();
        });
      }).on('error', (e) => {
        log.critical(`error fetching from CouchDB ${couchURL}`);
        response.writeHead(502, {
          'Content-Type': jsonp ? 'application/javascript' : 'application/json'
        });
        response.write('{"error":"DB_read_failed"}');
        response.end();
      });
    } else if ((location.pathname === '/config.json') && request.method === 'GET') {
      jsonp = params && params.callback;
      log.debug('***GET ' + location.pathname);
      response.writeHead(200, {
        'Content-Type': jsonp ? 'application/javascript' : 'application/json'
      });
      jsonString = JSON.stringify({
        port: 80    // lie that we are on port 80
      });
      if (jsonp) {
        jsonString = params.callback + '(' + jsonString + ')';
      }
      response.write(jsonString);
      response.end();
    } else if ((location.pathname.indexOf('/_in/') === 0) && request.method === 'POST') {
      let status = 200;
      jsonp = params && params.callback;
      log.debug('POST: params:', params);
      //log.debug('POST: location:', location);
      //log.debug('POST: request: ', util.inspect(request));
      try {
        body = JSON.parse(body);
      } catch (e) {
        log.alert('incoming webhook JSON parsing error', e);
        body = {};
      }
      log.debug('POST: body:', body);
      if (!body.token || (body.token !== options.RocketChat.token)) {
        log.alert(`bad incoming webook token ${body.token}`);
        status = 403;
      } else {
        saveNote(body);
      }

      response.writeHead(status, {
        'Content-Type': jsonp ? 'application/javascript' : 'application/json'
      });
      jsonString = JSON.stringify({});
      if (jsonp) {
        jsonString = params.callback + '(' + jsonString + ')';
      }
      response.write(jsonString);
      response.end();
    } else {
      file.serve(request, response, (err, result) => {
        if (err) {
          let headers = err.headers;
          headers['Content-Type'] = 'text/plain';
          log.warning(`Error serving ${request.url} - ${err.message}`);
          log.warning(`status: ${err.status} headers:`, headers);
          response.writeHead(err.status, headers);
          response.write("404 Not Found\n");
          response.end();
        }
      });
    }
  }).resume();
}

var bayeux = createBayeuxServer();
// require a token on proper messages
bayeux.addExtension(ServerAuth);
bayeux.getClient().addExtension(ClientAuth);

if (options.server.port) {
  let httpServer = http.createServer(httpHandler);
  bayeux.attach(httpServer);
  httpServer.listen(options.server.port, options.server.ip);
  log.notice(`Server started on ${options.server.ip}:${options.server.port}`);
}

if (options.server.secureport) {
  let sslOptions = {
        key: fs.readFileSync(options.server.privkey),
        cert: fs.readFileSync(options.server.fullchain)
      },
      httpsServer = https.createServer(sslOptions, httpHandler);
  bayeux.attach(httpsServer);
  httpsServer.listen(options.server.secureport, options.server.ip);
  log.notice(`Secure server started on ${options.server.ip}:${options.server.secureport}`);
}

