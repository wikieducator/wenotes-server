# WEnotes-server

This is a simple NodeJS-based web and [Faye](http://faye.jcoglan.com)
server used for WEnotes. Its main purpose is to allow WEnotes
clients to subscribe to topics (channels, tags, rooms) of
interest.

It also serves as a simple proxy to the backing CouchDB server
so that all requests can happen on port 80 (avoiding problems with
networks that filter other ports).

And its simple webserver provides an incoming endpoint for
integrations with [Rocket.Chat](https://rocket.chat) (or
presumably Slack or Mattermost, but those are untested).

`server.js` is intended to be run under a supervisor such as
[forever](https://www.npmjs.com/package/forever) or
[pm2](http://pm2.keymetrics.io/).

## License
MIT

